package ordersystem;

import java.util.Map;
import java.util.*;

/**
 * This class uses a Order object to construct a "full order" object with more information
 *
 */

public class FullOrder {	

    	
    	private long customerNumber;
    	private String firstName;
    	private String lastName;
    	private ArrayList<FullLine> fulllines;
    	private double totalOrderPrice;
    	
    	
    	public FullOrder(Order order) {
    		
    		Map<Long, Customer> customerDb = Database.getCustomerDb(); 
    		
    		customerNumber = order.getCustomerNumber();
    		firstName = (customerDb.get(customerNumber)).getFirstName();
    		lastName = (customerDb.get(customerNumber)).getLastName();
    		
    		totalOrderPrice = 0;
    		
    		fulllines = new ArrayList<FullLine>();
    		
    		for (long i = 1; i <= (order.getMapLines()).size(); i++) {
    			FullLine onefullline = new FullLine((order.getMapLines()).get(i));
    			
    			fulllines.add(onefullline);
    			
    			totalOrderPrice += onefullline.getTotalLinePrice();
    			
    		}  		
    		
    		
    		
    	}
    	
    	
        public long getCustomerNumber() {
    		
    		return customerNumber;
			
		}
    	
    	
    	public String getFirstName() {
    		
    		return firstName;
			
		}
    	
    	
        public String getLastName() {
    		
    		return lastName;
			
		}
    	
    	
    	
    	
    	public double getTotalOrderPrice() {
    		
    		return totalOrderPrice;
    	}
		
    	
    	public ArrayList<FullLine> getFulllines() {
    		return fulllines;
    	}
    	
    }


