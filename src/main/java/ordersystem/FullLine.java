package ordersystem;

import java.util.Map;
import java.util.*;


/**
 * This class uses a Line object to construct a "full line" object with more information:
 * description, unitPrice, totalLinePrice 
 *
 */


public class FullLine {
	
	   
     

    	
    	private long sku;
    	private String description;
    	private double unitPrice;
    	private int quantity;
    	private double totalLinePrice;
    	
    	
    	public FullLine(Line oneLine) {
    		
    		Map<Long, Product> productDb = Database.getProductDb();
    		sku = oneLine.getSku();
    		description = (productDb.get(sku)).getDescription();
    		unitPrice = (productDb.get(sku)).getUnitPrice();
    		quantity = oneLine.getQuantity();
    		totalLinePrice = unitPrice*quantity;
    	}
    	
    	
    	public long getSku() {
    		
    		return sku;
    	}
    	
    	public String getDescription() {
    		
    		return description;
    	}
    	
    	public double getUnitPrice() {
    		
    		return unitPrice;
    	}
    	
    	public int getQuantity(){
    		
    		return quantity;
    		
    	}
    	
    	public double getTotalLinePrice() {
    		return totalLinePrice;
    	}
    	
    	
    	
    	
    }
	
	


